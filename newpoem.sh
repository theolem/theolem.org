title=$1
date=`date --rfc-3339=date`
slug=`echo $title | tr '[:upper:]' '[:lower:]'|sed 's/\ /-/g'|sed 's/é/e/g'|sed 's/è/e/g'|sed "s/'/-/g"`

echo "Title: $title"
echo "Author: theo"
echo "Date: $date"
echo "Modified: $date"
echo "Category: poèmes"
echo "Tags:"
echo "Slug: $slug"

touch "./content/poems/$slug.md"
echo "Title: $title" >> "./content/poems/$slug.md"
echo "Author: theo" >> "./content/poems/$slug.md"
echo "Date: $date" >> "./content/poems/$slug.md"
echo "Modified: $date" >> "./content/poems/$slug.md"
echo "Category: poèmes" >> "./content/poems/$slug.md"
echo "Tags:" >> "./content/poems/$slug.md"
echo "Slug: $slug" >> "./content/poems/$slug.md"
