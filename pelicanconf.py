#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'theo'
SITENAME = 'theo lem'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

DEFAULT_ORPHANS = 0
PAGINATED_TEMPLATES = {'category': 10}
DEFAULT_PAGINATION = 10

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# # Blogroll
# LINKS = (('Pelican', 'http://getpelican.com/'),
#          ('Python.org', 'http://python.org/'),
#          ('Jinja2', 'http://jinja.pocoo.org/'),
#          ('You can modify those links in your config file', '#'),)

# # Social widget
# SOCIAL = (('You can add links in your config file', '#'),
#           ('Another social link', '#'),)

DEFAULT_DATE_FORMAT = '%a %d %B %Y'

CSS_FILE = 'main.css'
THEME = 'themes/theo_lem'

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Custom Jinja2 filter to get the Index page from Markdown
# From https://siongui.github.io/2016/02/19/pelican-generate-index-html-by-rst-or-md/
def pages_get_page_with_slug_index(pages):
  for page in pages:
      if page.slug == "index":
          return page

JINJA_FILTERS = {
    "pages_get_page_with_slug_index": pages_get_page_with_slug_index,
}

