Title: Retour sur la manif #NousToutes du 10 juillet 2020 à Lyon
Date: 2020-07-11
Category: blog
Tags: article
Slug: noutoute10072020endirect
Authors: theo



_Le 10 juillet 2020 à 18h30, une manifestation organisée par le collectif [Nous toutes](https://fr.wikipedia.org/wiki/Collectif_NousToutes) partait de l'Hôtel de Ville à Lyon. La revendication : les démissions de Gérald Darmanin et Éric Dupont-Moretti, nommés respectivement ministres de l'Intérieur et de la Justice lors du récent remaniement ministériel. Darmanin est actuellement sous accusation de viol, et Dupont-Moretti s'est montré ouvertement hostile au mouvement #MeToo. Rapide vécu de la manifestation._



**6h47** : Y'a une meuf qui a un tatouage de guillotine.

**6h48** : On doit être 800-1000 à vue de pif.

**6h48** : Cortège #NousToutes et LGBT+ en vue, mais aussi UCL et CNT.

**6h50** : pas besoin d'être beaucoup pour bloquer la rue de la Rep', comme quoi.

**6h50** : « Ministres, violeurs, assassins »

![rue république](/images/manif1007/image4.JPG)

**6h51** : Bifurcation aux Terreaux

**6h53** : y'a des taxis véners

**6h55** : perso, toujours autant de mal à être vocal en manif.

**6h55** : tiens, on s'est arrêté⋅es. Un feu rouge ?

**6h57** : « PatriarCACA, marre de ce quinquennat ». Prix perso de la meilleure pancarte.

**7h00** : « A-ha, anti, antipatriarcat »

**7h00** pancarte : « 2 loups sont dans la bergerie »

![quais](/images/manif1007/image3.JPG)

**7h02** sur les quais : il fait bon, y'a du vent. On est pas bien là ?

**7h06** pancarte : « Les accusations de viol détruisent tellement de vies qu'on en devient ministre.»

**7h10** : manif au rythme un peu en accordéon, mais néanmoins vive et bonne ambiance. Je like, je partage.

**7h11** : arrivée au pont Wilson, oh tiens l'Hôtel-Dieu qui se dessine.

**7h15** pancarte : « Présomption d'innocence = présomption de non-crédibilité des victimes. » Ça va jaser chez les bourgeois.

**7h17** : c'est la mode les deux chignons pour les mecs aux veuch longs ? Je suis tellement peu in.

**7h20** : oh y'a une pote de lycée !

**7h23** devant l'Hôtel-Dieu : mon tout premier die-in !

**7h28** : en tapant sur les échaffaudages, on a l'impression d'être légions. Arrivée à Bellecour.

**7h29**

![bellecour coucher soleil](/images/manif1007/image2.JPG)

**7h30** pancarte : « Marre de voir le mâle partout »

**7h37** sous la statue de Bellecour : « Antionomie », le reste je comprend pas.

**7h39** : alors, on la déboulonne cette statue ?

![satue bellecour](/images/manif1007/image1.JPG)

**7h43** pancarte : « Merci les mecs d'être là <3 » Quelle délicate attention.

**7h44** : bah voilà, c'est fini, les gens sortent les 86. Over and out, à vous les studios.

