Title: 5 albums
Date: 2020-11-18
Category: blog
Tags: article
Slug: 5albums
Authors: theo

<img class='subsection-banner' alt='bandeau1' src='{static}../images/5albums_bandeaux/fluid.png'>
## Royal Krunk Jazz Orchestra - Pyramids

Si je n'ai pas encore écouté la musique de son créateur (le trompettiste [Russell Gunn](https://fr.wikipedia.org/wiki/Russell_Gunn)), tout cet album me fascine. L'Égypte visitée dans l'album rappelle Sun Ra ou Pharoah Sanders, mais l'orchestration est beaucoup plus directe et cinglante, et le tout est d'une énergie folle.

J'aperçois aussi au passage Alex Han (saxophoniste de Marcus Miller) sur le [line-up](https://theroyalkrunkjazzorkestra.bandcamp.com/).

[![soundwave icon]({static}../images/icons/listen.svg)](https://www.youtube.com/watch?v=Zc_JB7DR-1M)


<img class='subsection-banner' alt='bandeau2' src='{static}../images/5albums_bandeaux/fluid2.png'>
## Pole - Fading

Découvert via le [site Pitchfork](https://pitchfork.com/reviews/albums/pole-fading/). Le mariage glitch / dub est fait de manière très maline, et les textures hybrides qui en ressortent me plaisent beaucoup : du rythme qui prend son temps, des grésillements tendus.

Selon [la page Wikipédia de Stefan Betke](https://en.wikipedia.org/wiki/Pole_(musician)), l'humain derrière Pole :
> Born in Düsseldorf, Pole took his name from a Waldorf 4-Pole filter, which he accidentally dropped and broke in 1996. Though the filter was perhaps no longer appropriate for DJ work in its damaged state, Betke found the strange hissing and popping noises the filter now made interesting sounds. He then began using the broken filter to create music, launching his musical career.

Ça me rappelle la conférence géniale de [Romain Constant](http://www.romainconstant.com/) que j'ai eu la chance de voir il y a quelques années, où ce dernier avait sorti des sons terrifiants d'un vieux synthétiseur resté trop d'années dans une cave trop humide.

[![soundwave icon]({static}../images/icons/listen.svg)](https://www.youtube.com/watch?v=c0qSLCf_a1k)

<img class='subsection-banner' alt='bandeau3' src='{static}../images/5albums_bandeaux/fluid3.png'>
## Loyle Carner - Yesterday's Gone

J'avais déjà entendu ce rappeur Anglais sur le très bon « [What am I to do](https://www.youtube.com/watch?v=q-xpm4lpNX4) » de Ezra Collective.

J'ai bien retrouvé sa sensibilité sur ses albums solo, où chaque texte sonne comme une confession. [Son passage sur COLORS](https://www.youtube.com/watch?v=wvbfpR7XOwQ), quelques mouvements de mains anxieux et le regard fuyant, ajoutent encore à mon amour pour le personnage.

[![soundwave icon]({static}../images/icons/listen.svg)](https://www.youtube.com/watch?v=pd2LDeFhj-U)

<img class='subsection-banner' alt='bandeau4' src='{static}../images/5albums_bandeaux/fluid4.png'>
## Bachar Mar-Khalifé - Ya Balad

Découvert par hasard dans la bibliothèque d'à côté de chez moi. Toutes les instrus de l'album ont ce côté millimétré, réduites à une substance indiscutable. Dans cette ambiance dépouillée, je me perd totalement dans sa voix, qui semblent sans fond, venue d'une gouffre.

[![soundwave icon]({static}../images/icons/listen.svg)](https://www.youtube.com/watch?v=N6ogh9_Q5NY)

<img class='subsection-banner' alt='bandeau5' src='{static}../images/5albums_bandeaux/fluid5.png'>
## PUP - This Place Sucks Ass

Une autre découverte de Pitchfork. J'ai très peu écouté de punk/rock depuis mes années de lycée, et j'aurais encore récemment dit que ce style m'ennuyait un peu, sauf peut-être en live. Puis récemment, sans trop savoir pourquoi, je m'y suis remis : du vieux (Pavement, CAKE, Dinosaur Jr, My Bloody Valentine), et du neuf, comme PUP.

Je n'aime pourtant pas ces styles pour la même raison qu'à l'adolescence : c'était les hymnes antisystème qui me parlaient à l'époque, c'est plutôt l'expression directe et sans fioriture que je trouve courageuse maintenant, cette manière si nécessaire de hurler qu'on va mal. Surtout chez PUP, où les inadaptés ont souvent la parole.

[![soundwave icon]({static}../images/icons/listen.svg)](https://www.youtube.com/watch?v=PWNyNg3SVqk)

_Et bientôt peut-être [une page Wikipédia](https://en.wikipedia.org/wiki/Draft:This_Place_Sucks_Ass_(EP))._


---
_Pour les bandeaux, j'ai utilisé le très rigolo [WebGL Fluid Simulation](https://paveldogreat.github.io/WebGL-Fluid-Simulation/)._
