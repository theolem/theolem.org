Title: Un festival et un calendrier
Date: 2020-06-01
Category: blog
Tags: article
Slug: weareoneff
Authors: theo

_Ces derniers temps, confinement aidant, j'ai passé beaucoup de temps à regarder des films. À l'occasion d'un festival de cinéma en ligne proposant de nombreux films issus de sélections de divers festivals (Cannes, Venise, Guadalajara, etc.), j'ai passé quelques heures à compiler leur programmation dans un calendrier._

## Le We Are One Film  Festival

En ce moment se déroule le [We Are One : A Global Film Festival](http://www.weareoneglobalfestival.com/), un festival de film en ligne. Pas une première dans le genre, mais sans aucun doute dans l'échelle : plusieurs dizaines de films sont disponibles en ligne, [directement sur Youtube](https://www.youtube.com/weareone), avec des « projections » en live, qui restent disponibles pendant toute la durée du festival (10 jours). L'appel au don est lancé, de manière assez originale : à chaque vidéo, un lien vers la plateforme de dons d'une ONG différente est proposé, directement dans la description de la vidéo. Le dénominateur commun : les dons serviront à la lutte contre le Covid-19.

Pas de commentaire sur le choix de la plateforme Youtube, qui a tous les défauts qu'on lui connaît. Je valide néanmoins le projet de laisser au grand public l'accès à un si grand nombre de films, même pour un temps limité. (Qui sait, peut-être que dans quelques années un festival de cinéma décentralisé de films sous Creative Common prendra place sur des instances [PeerTube](https://joinpeertube.org/) ?)

Je n'ai pour le moment pas eu l'occasion de regarder beaucoup de ces films, seulement quelques courts-métrages parmi les nombreux programmes proposés. On remarquera notamment une [sélection](https://www.youtube.com/playlist?list=PLA_atH--hPG5N0dlZhN4yGdCL07plyLxb) de films d'animation dont une grande partie provient du festival de films d'animation d'Annecy.

## Le (petit) problème

La programmation, disponible sur [le site internet](http://www.weareoneglobalfestival.com/schedule) n'est compilée nul part d'une manière satisfaisante (exportable et adaptable aux applications utilisées). Pour avoir accès à l'information « quel film est disponible quel jour à quelle heure », il faut donc aller flâner sur le site, repérer les films souhaités, y revenir...

J'utilise personnellement des applications de calendrier qui permettent de compiler ce genre d'information : l'application Agenda de Gnome pour mon PC sous Ubuntu, et celle de Nextcloud sur mon instance personnelle pour accéder aux différents calendriers de plusieurs appareils en même temps. Ces applications sont capables de lire des fichiers iCalendar (extension en **.ics**), qui contiennent des informations simples sur des événements : date et heure de début et de fin, titre, résumé, informations de lieu...

J'ai donc passé quelques heures à écrire un script qui crée ce fichier iCalendar à partir des informations du site (les dizaines de films disponibles m'ont découragé de le faire à la main.)



## La (rapide) solution

Le fichier de calendrier est disponible [ici](https://theo-lem.org/assets/waoff-calendar.ics), si vous souhaitez l'ajouter à votre application de calendrier. N'hésitez pas à me faire des retours sur son format, qui n'est sans doute pas parfait.

_Je vous conseille **fortement** de désactiver les notifications de ce calendrier après import, il y a beaucoup, beaucoup d'événements._

Le script que j'ai écris est disponible [ici](https://git.jean-cloud.net/theolem/waoff_parser), afin que chacun⋅e puisse l'inspecter. Rien de très notable (ni de très propre d'ailleurs), mais quelques points d'attention :

1. **Le site est basé sur un framework JS** (a priori React) : ça ne m'a pas facilité la tâche, car un simple `wget` ne permet pas de capturer la page HTML dans son intégralité. Il faut en effet charger le code JS et attendre qu'il crée la vue côté client, et je n'ai pas trouvé de manière rapide et simple de le faire de manière programmatique, en substitution du navigateur. J'ai donc dû contourner le problème en exécutant le code suivant dans la console de mon navigateur : `console.log(document.getElementsByTagName('html')[0].innerHTML)`, puis en copiant-collant le résultat (la vue côté client) dans des fichiers HTML, que j'ai ensuite parsé.

   _(Selon moi l'utilisation d'un framework JS pour ce genre de site est très discutable : c'est très « cool » et dans l'air du temps, mais ça ne fait que rendre le site plus lourd alors que je n'ai trouvé aucun élément dynamique sur le site, ni même d'animation qui justifierai de près ou de loin le chargement de tant de code dans le navigateur à chaque visite du site.)_

2. **Les horaires EST** : j'ai utilisé la très bonne librairie Python [pytz](https://pypi.org/project/pytz/)  pour convertir l'horaire de base affichée sur le site (Eastern Standard Time, soit -6h par rapport à la France) dans le fuseau horaire local. Le script permet d'ailleurs de choisir un autre fuseau, avec un comportement discutable (il faudrait par exemple proposer les fuseaux horaires sous forme de liste à l'utilisateur du script, plutôt que lui demander de les entrer à la main). J'ai également dû corriger à la main l'erreur d'une heure entre l'horaire EST et Europe/Paris, qui était dûe au changement d'heure, qui n'est pas pris en compte par la librairie.

3. **La librairie iCalendar** de Python, dont la doc est [ici](https://icalendar.readthedocs.io/en/latest/) : elle est super. Rien à dire.



En bref, environ deux heures de travail pour un fichier qui marche plutôt pas trop mal àmho. 

Bon visionnage !