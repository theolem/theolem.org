Title: Ce que ça fait de moi
Authors:theo
Date: 2018-09-05
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: ce-que-ca-fait-de-moi
Gorge serrée.  
Étalement de peine, yeux qui papillonent  
souffle court comme la pensée.  
C'est les regards perplexes  
c'est la fatigue  
c'est les grands films qu'on s'est fait  
les longues soirées qu'on s'est tapées  
mais plus que ça  
c'est le Bien  
c'est le Mal  
c'est l'histoire qui nous a mis là  
la force qu'on avait  
celle d'aimer  
qu'on a plus, qu'on a perdu  
qui nous a échappée parce qu'un matin  
on s'est réveillés  
on s'est regardés et on a vu l'un dans l'autre  
le plus piteux des acrobates  
un oisillon naissant transformé en iguane immonde  
c'est le pro c'est l'anti  
le para le post le neo l'alter  
qui nous mire dans les pupilles  
pour nous convaincre de ses iris aqueux.  
  
C'est la honte qu'on sême  
qu'on ramasse en trainant nos haillons blancs rayés noirs.  
  
Ça fait de moi un type qui caresse des idées  
dans le fond de sa grotte, dans un coin de l'allée  
ça fait de moi le placide - ou ça le fera bientôt.  
  
Alors je prie de ne plus jamais prier.  
Je prie pour que subsiste cette ardeur  
    ces mots qui vomissent  
    cet entrechoc entrelaçé  
    cet imparfait qui m'inonde  
    qui me couvre de poils  
  
alors je serais l'auroch  
le golem  
le berserk  
l'atteint  
que cette honte me donne encore fierté  
et de fierté je ferais festin  
    dix tables, mille chaises  
    cent plats et de la viande  
    sanguinolante en dessert  
  
on se jettera des coups  
au nom des séries pourries  
du temps perdu  
des grands regrets  
  
ça fera de nous des sans-noms  
    avec des casques en fer  
    et des coeurs volants. 


