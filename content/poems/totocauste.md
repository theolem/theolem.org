Title: Totocauste
Authors:theo
Date: 2023-09-18
Modified: 2023-09-18
Category: poèmes

tu verras

on sera toustes poête⋅sses au milieu des météores

on en aura plus rien à foutre en on en pleurera.

il y aura celleux qui marmonneront tout bas et même les dernières brûlures n'entendront pas ce qu'iels avaient à la bouche dans leur dernier cri

mais le reste saura enfin gueuler.

<br>

alors que la chaleur vaporisera les villes

et que le vent projettera les nuages

et que les nuages geindront sous le poids du ciel

nouvellement alourdi

nous excréterons tout ce qu'il restera à râcler dans le fond de notre être

là où les mots sédimentent sous des couches de fluides

ça inondera nos corps pour quelques radiations lumineuses

et on s'enroulera

s'enroulera

s'enroulera

---

brève la nuit

bref le jour

vois comme les mots sonnent

sens comme les sons fuient

bref le jour

brève la nuit
