Title: Dans la rue
Authors:theo
Date: 2021-03-16
Modified: 2021-03-16
Category: poèmes
Tags:
Slug: dans-la-rue

je suis sorti marcher

alors que la nuit tombait à peine

et que le jour

ne s'éclipsait pas encore.

<br>

dans la rue

toi

et moi.

<br>

l'emprise du froid

et

le chaud sous cloche.

<br>

derrière les vitres

la vie sans masque

et la mort massive.

<br>

sous les torrents de lumière jaune

en fendant l'air d'un ton pressé

en faisait claquer mon manteau

j'ai de nouveau repris ma marche

ressassant de vieilles pensées

qui m'empêchaient de trouver le sommeil.
