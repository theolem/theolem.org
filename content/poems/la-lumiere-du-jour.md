Title: La lumière du jour...
Authors:theo
Date: 2017-01-27
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: la-lumiere-du-jour
La lumière du jour  
m'emplâtre  
la lumière de la nuit  
m'encastre  
j'emmène avec moi quand je passe la porte  
un petit peu de cet air  
saoûlé  
qui pulse dans mes tempes  
je marche en mâchonnant dans les rues de Lyon.  
Plus le temps passe  
plus je me sens raccourci  
plus réticent  
moins habité  
je deviens un petit animal  
le coeur à cent-soixante  
jusqu'à ce que les crocs m'en poussent  
et alors mes lèvres se fendent  
je laisse perler le sang  
il éclabousse le trottoir  
qui s'en souciera  
je retrousse les babines  
et sur les bords du Rhône  
j'ai franchi le pas de la léthargie  
à la rage  
montrez-moi un homme fort  
je vous en brosserai le portrait  
à la gouache et à la bile noire  
et quand je rentrerai ce soir  
puisse la nuit me porter conseil  
sinon j'aurai vos têtes.


