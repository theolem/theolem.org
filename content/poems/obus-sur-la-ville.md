Title: Obus sur la ville
Authors:theo
Date: 2017-01-27
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: obus-sur-la-ville
Il y eu un souffle  
qui m'a empreint de toi  
du tout-toi  
et dans la courbe ultime de l'instant  
j'ai enfin cru entendre  
l'air ruisseler sur ta peau  
entendre tes yeux  
comprendre tes os  
m'aspirer ta peur  
m'envoyer ton oubli.  
S'il y a une chance  
une fois l'éclatante chaleur  
passée  
qu'un éclat du maintenant  
du ici  
du toi  
s'en aille ricocher dans le néant  
réveiller ce qui y sommeille  
je serais heureux.  
J'aurais vécu parmi toi  
et même dans cent ans  
on retrouvera la même image gravée sur ma rétine :  
celle de nous deux, volant,  
marchant sur un souffle  
qui nous aura été cher  
ne fusse qu'un temps  
n'ayant plus rien d'autre à savoir  
que de se perdre.


