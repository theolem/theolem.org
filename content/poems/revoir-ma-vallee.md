Title: Revoir ma vallée
Authors:theo
Date: 2017-09-03
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: revoir-ma-vallee
Revoir ma vallée,  
j'en pleurerai pour revoir ma vallée.  
Son visage  
entre poupon bouffi et tendre ailleul  
oh  
oh oh oh  
elle que j'ai pu voir d'en haut  
depuis les nues presque quand je souhaitais  
l'embrasser d'un oeil  
elle qui me berce de ses sons intestins  
et moi qui manquais à l'appel.  
  
Demain c'est décidé  
vous pourrez me chercher  
dans chaque pièce de  
chaque maison de  
chaque village  
vous ne me retiendrez jamais  
et moi même il me semble que je dois  
prendre congé  
me laisser dorloter entre le coins jaunis  
d'une vie qui ne m'a rien appris.  
 J'irai revoir ma vallée  
pas comme on vient demander conseil à un sage  
mais comme on rechante une vieille chanson  
elle jaillira à mes yeux  
comme de vieux couplets  
ma vieille vallée.


