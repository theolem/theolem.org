Title: Ce dont je rêverai demain
Authors: theo
Date: 2021-03-25
Modified: 2021-03-25
Category: poèmes
Tags: 
Slug: ce-dont-je-reverai-demain



alors que les voix me submergeaient et que la pièce suffoquait doucement

je me suis vu plus sage que les sages

plus loin que le temps qu'on peut compter.

<br>

la droiture de mon dos n'avait d'égal que la sévérité de mon front

émacié par le combat

car la faim n'accouche jamais de fils moribonds.

<br>

la lumière a coulé comme à travers les vagues

en un rideau ethéré qui m'a fait cligner des paupières.

<br>

la douce familiarité a battu mes tympans comme des algues

encore une fois rabattues contre leur socle de pierre. 

<br>

j'irai demain marcher

pour que le vent dur du dehors ponce ma peau

et que les tourmentes

nettoient les ivresses et les millions d'erreurs

<br>

mais seul le soleil qui filtrera à a toute première heure

au travers de mes paupières

saura ce dont demain je rêverai.