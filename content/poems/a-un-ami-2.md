Title: A un ami (2)
Authors:theo
Date: 2014-12-30
Modified: 2020-05-07
Category: poèmes
Tags:
Slug: a-un-ami-2


4\. Il est charnel

que de s'en satisfaire.

Ami, je ressens tes dents enfoncées dans mon échine

quand je vois tes doigts de fée

est-ce un blâme qu'on me jette alors

à fleur de peau dégarnie ?





5\. Un homme en bois de femme

qui brûle au moindre encens sans se pétrir de peur

mais qui ne fait que poser ses orbites creuses comme une statue sans bras sur

un creux vague de mon cou

ou alors un homme de plume

enfoui au fond d'un nid de débris de fonte.

Le choix n'est pas large – hélas.



6\. Qui vogue fredonne,

qui délire sue,

qui mord et grogne,

qui meurt détonne

son étendue.




