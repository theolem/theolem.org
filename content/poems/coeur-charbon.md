Title: Cœur charbon
Authors: theo
Date: 2020-09-04
Modified: 2020-09-04
Category: poèmes
Tags:
Slug: coeur-charbon



le rimmel qui tag les murs

s'écoule

Grenoble le rideau de pluie est lourd

et un instant il m'a parut t'arrêter

<br>


ça me ferait le plus grand bien

si ça tombait encore

des siècles d'eau claire

enfin s'abattant en ruisseaux

<br>

une trace de doigts

marque légèrement ma peau

un rayon d'ombre barre mon front

et au travers de ce temps engourdi

je traîne mon âme

<br>

j'aspire une goulée d'air

et la ville m'envahit

des cristaux de son charbon

se logent entre mes dents

\-

yeux grands ouverts

qui fixent la ville immobile en contrebas

que voyez-vous ?

<br>

les phares qui peinent à percer le jour

les victimes blotties

de ce millième de drame

les fuyants urbains qui ont déjà oublié

quel goût a un jour d'été.
