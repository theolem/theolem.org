Title: The woods
Authors:theo
Date: 2014-08-28
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: the-woods
Deep inside the dark vault  
amongst and beneath the crumbs of last Autumn  
lies a dreaming human stump  
as if he were deceased  
as if he were faulted  
  
over his tanned wrinkled skin  
victim of winds and feasting dragonflies  
rolls the residues of the last  
thousand evenings' fogs  
as if he had been dreaming  
  
here with the drops of melting ice  
circling his lips as the years slided by  
perhaps conveying an inch of a thought  
in the canyons of his torturous brains  
on New Year's Eve, sometimes.  
  
the muddy factory of souls that lies  
underneath his legs and arms  
for that while has worked him  
with the tools nature provides  
with the time it saw running.  
  
Now as soon as the morning light  
pierces through the misty heights of the Atlas mounts  
finally the mandibles will cut and slice  
and tear for the grinning to become  
a house for a thousand souls.  
  


