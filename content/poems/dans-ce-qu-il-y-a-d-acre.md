Title: Dans ce qu'il y a d'âcre
Authors:theo
Date: 2023-08-30
Modified: 2023-08-30
Category: poèmes

je fume en attendant que tu répondes

dans ce qu'il y a d'âcre je trouve une consistance

dans mes yeux tu pourrais voir comme ça me pique que la nuit soit déjà là.

<br>

hier soir j'ai écris sûr

pas une hésitation dans les traits que j'ai tracé.

Ça disait le vacarme quand ta simple vue faisait péter les entourages

et ta bouche vrillée de ratures édentées.

<br>

la fumée fait son chemin vers mon nez et m'éclate trois vaisseaux

si le sang passe encore par là à l'heure qu'il est il doit être chargé de ces toxines qui donnent des prises à la vie.

je sens se générer en moi une envie d'englober ce qu'il y a à englober et de dérober le reste

se génère en moi toute l'ivresse sale de nos soirées humides à déverser nos signes sur l'extérieur - l'intérieur est comme la cahute qui contient l'orage

<br>

et c'est dans la cahute que s'éteint la dernière lumière du soir

toute clope éteinte, toute détresse bue

laissant à la nuit la fumée et les toxines.
