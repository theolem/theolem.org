Title: C'est une bien longue nuit
Authors:theo
Date: 2017-05-05
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: c-est-une-bien-longue-nuit
C'est une bien longue nuit  
pour s'en saoûler et s'en saturer les veines.  
Car avec toi  
sur les bords du Rhône  
j'ai appris que grandir  
c'est aussi savoir se renfermer  
sentir l'effluve après qu'elle ait été.  
C'est qu'avec toi sur les bords du Rhône  
si l'entendement n'avait pas rattrapé ma main  
je t'aurais sûrement prise car un cyclone  
traversait mon corps et me broyait les reins.  
C'est une belle journée  
aujourd'hui août me semble proche  
et la lune me semble loin.  
Aujourd'hui rien d'autre que ton rire  
ne me fera sourire  
et je n'aurais de cesse que de  
te sentir t'agiter en moi.


