Title: Mardi TLJ/TLH
Authors:theo
Date: 2018-09-05
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: mardi-tlj-tlh
Mardi 17h00.  
Les quais du Rhône sont pleins  
   de petits  
   de petits uns  
regardez-moi slalomer  
j'emplis l'autour avec vous.  
Partie et tout à la fois essence  
    \- [ savoir que vous étiez  
        et savoir qu'ils furent ] -  
vivre à la cadence -  
partie et tout à la fois en transe.  
  
Mardi 18h00.  
Cartons de bières vide et  
jeux amoureux - à cet endroit  
on m'a largué.  
Ça fait un an, ou moins.  
Ça m'amuse en fond, ça m'irrite.  
J'espère qu'on ne le lit pas sur mon visage.  
  
Mardi 18h24 le cours traîne cht cht cht  
esquive un camion ma perte mes voeux  
rien n'est plus comme  
liens coupés panique en boule  
renfermé, vert et jaune, bougon ce soir.  
  
Mardi, tous les jours, toutes les heures  
je déclare mon amour  
à ceux qui dynamitent l'ordre social  
et aux autres qui sont à ÇA de s'y perdre.  
Qui que vous soyez, sachez  
que je suis euphorique  
et qu'être parmi vous, c'est un honneur.


