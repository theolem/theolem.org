Title: Les immensités
Authors:theo
Date: 2021-03-01
Modified: 2021-03-01
Category: poèmes
Tags:
Slug: les-immensites

mon ami

il y a de ça des années

je me trouvais là où les immeubles ont poussé

puis se sont figés en figures harassées

fixées dans la stupeur de toutes ces immensités

et dures sous le soleil nu qui accablait notre petit monde.

<br>

mon ami

au milieu de ce décor qui ne laissait rien échapper

dense comme un trou noir et toxique au centuple

de mes yeux j'ai vu vaciller

ce qui jamais ne devait vaciller

et j'ai vu les immensités répondre aux cris des tambours.

<br>

mon ami c'était grandiose

en ce jour les rues furent emplies d'un vent nouveau

et lorsqu'il le frôlait le béton s'étirait

et dans les tours branlantes d'une danse de nuit blanche

l'éclat du verre brisé le disputait au crissement de l'acier.

<br>

mon ami

rien ne m'est arrivé de tel depuis

et en vérité il me semble que rien n'est arrivé de tel à personne

mais il me semble quand je te dis ces mots

que ces mots en moi-même chantonnent.
