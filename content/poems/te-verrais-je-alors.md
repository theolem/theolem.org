Title: te verrais-je alors ? 
Authors:theo
Date: 2014-10-22
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: te-verrais-je-alors


te verrai-je alors ?

si je m'exile en longues quêtes

si je ramasse et j'époussette les dix mille morceaux de ton horizon

et si je les recoud patiemment

et m'ouvrant les pouces sur les bords coupants

  

te verrai-je alors ?

si jamais je culbute par-dessus la Terre en écartant les bras

si j'englobe le monde et si je t'enveloppe toi

si je tapisse tes murs et t'écoute enfin

comme une méduse invisible et vaseuse

si je te dévore sans que tu y penses

  

te verrai-je alors ?

cas si je m'accroche à la corniche sous la plante de mes pieds

et que je balance mes orteils loin vers le bas

si je hurle jusqu'à la foulure de mes tympans

et tambourine mon torse de chair noire

n'entendra-tu que les nuages, dis

  

te verrai-je alors ?

si je roule sur moi-même en descendant la pente

et je roule jusqu'aux eaux profondes comme un galet

et je roule plus bas encore

si j'en perd le sud du nord – alors

  

te verrai-je alors, te verrais-je alors ?

j'irai jusqu'aux fumées noires blanches et grisées

et me fondrai dans les feu-follets


