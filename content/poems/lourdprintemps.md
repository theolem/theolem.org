Title: Lourd printemps
Authors:theo
Date: 2024-06-07
Modified: 2024-06-07
Category: poèmes

Quelques jours de pause. Je retrouve mon rythme de lent chaos, tâches éparses sur un fond de vide continu.

Sérotonine, dopamine, adrénaline.

La musique, la lecture et le politique.

La vie, le futur et l'enfer.

Moi, les autres et la perdition.

Nature, culture, violence.

Douleur, bien-être et inconfort.

Printemps, été, nuages rauques indifférents.

S'égrainent comme des jours les angles d'attaque, j'observe là et tout coule à flot ici.

Rappelle-toi cet automne. Sous tes airs de chien battu quelque part au fond tu vibrais avec toute la ville.

Tu ne peux pas sentir cette vibration aujourd'hui ?

Cette communauté dans l'errance.

On trouve la ressource où on peut. Dans ce cycle de mélancolie-reconstruction doux comme une trajectoire d'insecte dans l'air brisé.

Ce printemps je me construis un art, une envie claire de tout bouffer.

Je construis le timbre pour qu'il résonne dans mon futur automne.
