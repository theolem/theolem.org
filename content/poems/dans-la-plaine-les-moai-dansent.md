Title: Dans la plaine les moai dansent
Authors:theo
Date: 2014-10-09
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: dans-la-plaine-les-moai-dansent
Dans la plaine les moai dansent  
se lèvent se tordent se fendent et tombent  
crispés ils tombes  
sans cadence s'élèvent  
sous le levant enfin  
tombent en masses mortes  
virevoltent sous les traits émaciés  
puis chancèlent encore  
en leur ether-rève fièvre  
vendredi saint qui jète les os  
samedi froid qui survole les corps  
les eaux chargées aux aurores  
vendredi dans la plaine les moai dansent  
se lèvent se tordent se fendent et tombent.  

  


