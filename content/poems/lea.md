Title: Léa
Authors:theo
Date: 2014-12-14
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: lea
Léa si je meurs un jour  
apporte moi dans un bol de terre noire  
un peu de cette eau qui ruisselle  
sur tes joues sur ton menton  
Léa apporte-moi l'idée de la Lune  
et le soupir du soir en hiver.  
  
Léa toi qui prend l'azote  
de l'air je t'en supplie  
étend étend vers moi ta main tes doigts  
de songe de manège de carroussel  
que j'y place dans le plus grand secret  
une poignée de terreau sombre  
qui est mon sang  
et qui fait ton rire.  
  
Oh Léa toi qui flâne encore  
toi que seuls les nimbus narguent encore  
toi qui me manque, toi qui m'assiège  
oserai-je jamais soulever ton corps ?  
Toi qui me rompt, toi qui me brise  
toi qui me casse, toi qui m'arrache  
saurais-je jamais que tu m'hypnotise -  
  
Léa dis moi si je meurs un jour  
puis-je peut-être t'envoyer quérir,  
et partir avant ta venue


