Title: Andréa (1) 
Authors:theo
Date: 2017-01-03
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: andrea-1
Je l'ai enfin devant moi, je la tiens.  
Oh, tu peux garder tes beaux yeux rivés sur le sol  
tu peux me fuir  
mais nous savons tous deux que tu échoueras  
Andréa  
n'est-ce pas ?  
Dans tes doigts qui tremblent  
qui trahissent  
tes membres qui s'agitent  
ton coeur qui bât la chamade  
ton esprit qui hurle  
Andréa  
il y a une Camel qui se consume  
et j'aime comment la fumée embrasse la nuit noire  
pour annoncer l'heure où les Apaches  
sortent  
de leurs huttes  
soudain pris  
du désir  
de  
te  
tuer.


