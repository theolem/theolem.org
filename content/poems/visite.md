Title: Visite
Authors:theo
Date: 2016-02-02
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: visite
J'aime croire qu'une chose parmi les autres  
pourtant  
reste et s'attarde. Hier soir j'ai vu mon amour  
pour la dernière fois.  
J'aurais pu trébucher et me perdre  
dans les pores de sa peau  
tant son toucher semblait fin et sans borne.  
J'ai senti couler sur moi tout à la fois  
l'envie sans appel  
les ongles dans la chair de mon dos  
un regard perdu  
                 et j'ai pu apercevoir enfin  
                        dans la fumée de sa bouche après l'amour  
                                 le plissement de ses lèvres.  
  
  
  
Pendant que je la regardai se grandir  
et étirer ses maigres ras  
l'image s'est fondue dans mon iris, a coulé  
dans mon cerveau et en hante maintenant  
les canaux et je sais qu'une fine  
                 couche  
                     de  
dentelle m'enserrera jusqu'au jour où  
je me payerai un avion pour plus haut encore.


