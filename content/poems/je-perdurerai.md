Title: je perdurerai
Authors:theo
Date: 2014-11-02
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: je-perdurerai


si c'est ainsi je perdurerai

à la verticale de l'horizon

> « Homme et de grande taille  
>
>
> quoique si petit vu de loin »,

je résiderai en l'espace.

  

si je suis un parasite mural en devenir

alors je perdurerai

soit un déchet plastique gluant

soit un lampadaire de fonte,

indélébile

> : « le moule de la vie intriqué enchevêtré se délace... lentement... »

  

mon amour au bord des certitudes

  

je reste sûr : je perdurerai.


