Title: le goût des jours perlés
Authors:theo
Date: 2023-10-10
Modified: 2023-10-10
Category: poèmes

je veux trouver le passage

je sais qu'il n'y a pas de mot juste

je respire en puisant la brume

le temps est court

je longe la tendre idée de m'offrir entier.

\*\*\*

le contexte est chaud et pesant

les moustiques fusent

au loin la lumière décline dans le sombre

on ne fait pas grand chose à l'heure où la peste s'envole

dans les odeurs de sueur

Petit regarde dans le vide

et son temps intérieur tourne à l'envers

il est une singulière représentation d'absence.

Il a lu et il pense avoir compris que

  l'étrangeté insidieuse renferme ce qui libère

  les mots soudains ont une valeur

  qu'on aime ce qu'on copie.

Dans ses yeux transparaît une vague cendrée.

Ses jours sont poudreux en ce moment et s'échappent par grappes

Petit boit trop et il s'accroche à ce qu'il a

il se dirait balloté par ce fleuve mais il sait qu'il s'y abreuve

ses jours sont courts et peu semble lui survivre.
