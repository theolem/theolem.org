Title: Journal
Authors: theo
Date: 2021-02-01
Modified: 2021-02-01
Category: poèmes
Tags:
Slug: journal

À la verticale des nuages qui évoluent à l'horizon

Montpellier dans ses dernières heures ressemble aujourd'hui à un champ de minarets

les pylônes ont parlé hier

et leur voix a fait décoller les derniers oiseaux que la saison

n'avait pas emporté.

<br>

cette rue se meurt

ça se lit sur son visage

aussi clair qu'une effusion de sang.

À chaque détour d'épaule

à chaque profil volé

c'est tout ce que je n'entend pas

mais qui résonne le plus.

<br>

qu'as-tu fait de ton amour

qu'as-tu fait de ta famille

qu'as tu fais du souvenir du temps où

tu n'étais pas recroquevillé ?

<br>

as-tu préféré humblement sourire,

te terrer derrière des larmes déjà sèches

au fond

as-tu pactisé avec ce malheur qui pèse

sur le champ de minarets ?

<br>

moi j'ai peur

peur d'avoir déjà un peu accepté, peur d'assister comme le plus démuni des témoins

alors que la roue de ma vie lâchée sur son essieu en libre rotation

oscille et tangue sur son axe

peur d'attendre le jour où il se brisera.
