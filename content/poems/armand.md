Title: Armand
Authors:theo
Date: 2023-08-23
Modified: 2023-08-23
Category: poèmes

Armand ce que tu ne sais pas

je vais le hurler à tes oreilles et j'en profiterai pour glisser mon numéro dans ta poche

la nuit s'enveloppera chaude autour de nous et peut-être que notre au-revoir aura le même goût de vieux sel pour toi que pour moi.

Armand mon hurlement il est né une après-midi d'août qui pique la peau. J'ai tendu l'oreille en brassant des bras puis les sons sont retombés comme un magnétophone cassé. Je ne savais pas quoi foutre de moi alors j'ai écrit.

Quand on écrit on s'enroule sur soi-même et à l'instant où notre nuque se brise on s'ouvre le ventre et quand on émerge les emmerdes ont fait un pas de côté.

Mon hurlement naît sous un stylo noir au fur et à mesure que je tire ce serpent entortillé dans mon corps.

je l'ai avalé il y a quelques années sans trop savoir

maintenant il bouge mes doigts et il attire ma langue vers tout ce qui palpite et qui dégouline.

<br>

Je reprends.

Dans une semaine viendra l'heure encore et je réécrirais et encore une fois mes mains finiront contre ma cuisse

je m'imaginerais te coulant sur moi avec l'été comme seul témoin

et j'imaginerais d'autres mots pour te dire comme je t'ai attendu.

<br>

Viens et graille moi comme la moiteur graille ma joie.
