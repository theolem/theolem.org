Title: Ants 
Authors:theo
Date: 2014-08-11
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: ants
i like the ants  
so silk- so clay  
tipping toes  
take away  
softly nose  
and as they stroll  
terribly small  
forget no smell  
clinging walls  
fast flurrying  
the stones  
the moss  
the hay  
forever around  
i see them fall  
the sun under  
the stones above  
terribly wall  
tipping on wells  
take away  
  
oh ants, take me away


