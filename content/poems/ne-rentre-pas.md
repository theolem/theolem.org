Title: Ne rentre pas
Authors: theo
Date: 2020-06-14
Modified: 2020-06-14
Category: poèmes
Tags: 
Slug: ne-rentre-pas



en remontant le canyon

hérissé de lourds rayons

qui assèchent et sucent jusqu'à l'os

des pauvres hères

habitant encore ces lieux désolés

\- ne rentre pas

je t'en supplie

c'est là la demeure

de celui qui à jamais est honnit

car c'est celui

qui apporta les tapis de neige

et les pluies de serpents

et les nuages noirs

si noirs que les âmes de nos morts

nous en sont parvenues

\- ne rentre pas

ne passe pas cette porte

il y a celle dont les doigts gouttent encore

les larmes de nos parents. 

sa voix suffit à égorger

l'espoir même d'un jour voir la lumière.



ne rentre pas.

toi dont la peau scintille

qui porte en toi le rythme du bois

qui tappe contre le bois

quand le vent

souffle au plus fort.

<br>

je sais qu'un jour 

ce vent aura raison des clous

et des chaînes

et en soulevant les planches

par derrière leurs interstices

il en arrachera jusqu'à la moindre écharde

et une silhouette glapira

en jaillira mais personne ne la verra. 

<br>

elle est déjà

perdue 

dans les ombres.  