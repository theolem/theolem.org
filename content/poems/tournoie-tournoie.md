Title: Tournoie tournoie
Authors:theo
Date: 2017-11-11
Modified: 2020-05-07
Category: poèmes
Tags:
Slug: tournoie-tournoie
Il trouve bon enfin
de se laisser aller au sommeil.
Il sait désormais qu'il est un lieu
parmi les années lumières de désert froid
où il pourra arriver tard le soir
et entrouvrant la porte
un ami de longue date l'apercevra
et il se lêvera pour le présenter
il entrera alors
on lui servira un verre
et il sera au chaud,
alors,
parmi les tapisseries tissées et
la fumée d'un joint.
On lui racontera les nuits d'automne
où les arbres roussis reprenaient leurs souffles
et où les montagnes d'Ardèche semblaient béantes.
On lui dira les pintes dans les pubs des Pentes
l'âcre brûlure du soleil de midi
le doux ennui qu'on enrichit ensemble.
On lui dira tout cela
et lui,
ces quelques bribes il s'en fera un
manteau de cuir et il
vêtira ses plus beaux atours et il
se lêvera, saluera toutes ces
chères bouches avenantes et il
se jettera dans le ciel
tournoie tournoie
homme qui pense
homme qui aime
tournoie tournoie et plâne
quand tu termineras de dévisager
chaque aspérité de ton petit monde
je garderai une place au chaud
et tu y seras à jamais le bienvenu.


