Title: Pourrir
Authors:theo
Date: 2017-02-03
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: pourrir
Faire de toi une boule de neige  
que fondra au creux de mes mains.  
Fléchir mes veines  
m'étrangler avec plutôt que de t'avoir  
vu  
j'aime  
le nu de ton corps  
qui affûte mes sens  
jusqu'à ce que je n'en dorme  
plus j'aime  
le gris du ciel qui pêse tant sur les yeux  
qui m'emproie  
me ponce à petit feu.  
On m'a attaché à une chaise en bois  
et quand l'impact des coups a quitté mon visage  
ce qu'il restait n'était pas  
un désert informe de sel et de chair  
plutôt l'étrange ébullition d'un coeur  
en froid  
avec les siens.  
On me jettera en pâture aux chiens  
avant que je ne me taise.  
Si j'aime la lumière  
qui me vient d'étoiles mortes depuis des lustres  
alors j'ouvrirai bien les bras  
à une tiède étreinte.


