Title: Viens résonner
Author: theo
Date: 2021-07-28
Modified: 2021-07-28
Category: poèmes
Tags:
Slug: rien-de-bien-grand

le trop grand appétit des choses

m'aspire de nouveau.

<br>

loin derrière l'horizon

j'entends les vagues se briser

les rouleaux vibrent avec fracas

et leur chant sonne à mes oreilles

comme une question indicible

ou une injonction sans prise

<br>

viens naviguer l'intangible

viens rejoindre l'indicernable

viens résonner dans les murmures

au sein desquels tu n'es rien de bien grand

au sein de quoi ne t'écoutent que des oreilles lointaines.
