Title: Nul espoir n'est perdu
Authors:theo
Date: 2017-11-11
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: nul-espoir-n-est-perdu
Nous sommes, mes amis, mes frères  
comme la fine droite de lumière qui  
transperça l'air  
ce matin  
et vint mourir en mes yeux  
nous sommes  
infiniment longs  
et infiniment petits  
en ces temps qui nous écrasent.  
Devant nos paupières, lorsqu'on veut bien les fermer  
se tient la dernière falaise sur la côte orageuse  
fière d'être à bout, et ridée de fierté.  
Que pouvons-nous  
alors  
?  
Ressentons-nous  
à peine au travers des vagues  
les cieux apaisés  
sans pli  
souriants  
?  
Fondons,  
et accrochons  
des drapeaux  
de couleurs au rocs qui  
nous tendent ostensiblement les bras  
dans notre chute finale  
qui disent en lettres d'argent  
"Bénis, bénis,  
soyez pardonnés là où nous ne le fûmes  
pas, soyez  
à jamais bénis.  
Puissions-nous un jour  
vous revoir  
en rire, en parler,  
nous avons chû  
vous avez vécu  
et la grâce de nos maîtres  
vous revient.  
Nul espoir n'est perdu.  
Nul espoir  
n'est perdu. "


