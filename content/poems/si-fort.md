Title: La fumée
Authors: theo
Date: 2020-06-01
Modified: 2020-06-01
Category: poèmes
Tags:
Slug: la-fumee



j'essaye si fort

de planter les fondations

d'un monde si dur

<br>


puis je vois

la lumière des lampadaires

comme des phares oranges sur l'eau tiède du Rhône

et la surcouche irritable de beauté sur les murs de ma ville

<br>

je vois les mots qui s'entremêlent en phrases

et qui sortent des lèvres de mes chers 

comme de la fumée de narguilé

<br>


et je vois un sourire tout de dents au détour d'une vanne qui n'est pas la mienne

<br>


le monde que j'aimerai construire

il est fait de briques de vents tendues d'étoffes légères

le temps

n'y fera rien

car chacun le traverse déjà

sans le voir

<br>


une stratosphère

pour stratophrases

enfilées de perles de verres

<br>


j'ai sué sangs et larmes

sur chaque goutte de mortier

mais déjà à la fenêtre me parvient une rumeur encore lointaine

mais que déjà je reconnais

c'est eux

c'est elle : l'immense vacuité

de tous ces rires et de tous ces coeurs

qui suintent de ne plus jamais dormir

toutes ces âmes en mal de simple sobriété

<br>


j'en recouvre ma peau

je frotte contre mes pores

cette sensation de désespoir fébrile et cru

teinté d'éclats

et d'asphalte pur.

elle pénêtre mes poumons

et elle s'étend

prend du volume

éclate

rampe dans mes boyaux

saccage mes capillaires

sature mes veines

puis jaillit

grande et belle et féconde.