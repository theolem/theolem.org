Title: Ravage
Authors: theo
Date: 2021-04-12
Modified: 2021-04-12
Category: poèmes
Tags:
Class: monospace-this
Slug: ravage


ravage sans une

fleur ni embrun

brise au dessus

du champ tonant

pas une rime au

fusil gémissant

tonnerre ni une

sagesse à cette

triste dame qui

crache les obus

striant le ciel


---

douce herbe sans âme

sans prise aux vents

sans spore à l'hiver

même où le froid dru

chamaille ton cheveu

de petites tempêtes.

---

lettre : à toi

à moi, fomenté

par la main du

plus proche de

tous les êtres

.

à moi de moi à

l'autre de toi

une missive en

partie finie :

s'oublier sans

désespoir, nul

orgueil face à

l'immense tour

au zénith, sud

pour le rêveur

nord pour tout

ce qui restera
