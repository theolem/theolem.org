Title: sale bête
Authors:theo
Date: 2024-02-13
Modified: 2024-02-13
Category: poèmes

c'est si vieux

si intimement ancien

ça ondule sur son dos

la grosse bête du monde.

<br>

nous serons trempés dans la violence

nous n'aurons pour nous que le bruit

&emsp;celui des ordres distants

&emsp;et celui de la roche.

<br>

il faudra apprendre l'alchimie

qui matérialise le vertige là où il n'y a que gravité

il faudra empiler des fioles

et savoir tenir quand les vannes lâcheront.

<br>

ancien comme un savoir

un jour quelqu'un a unifié toutes pensées

et les a concassées dans le mot "monde"

et nous qui baisions avec les constellations on s'est retrouv¦s à terre

on m'a arraché un membre

une excroissance

celle qui me faisait comprendre la ville

plonger mon regard

dans les montagnes.

<br>

il faudra tenir quand le souffle sera court

l'alcool ça aidera

et le soleil aussi

et l'image d'un cerveau dont chaque canyon déborde

d'une rivière submergeante
