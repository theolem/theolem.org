Title: Andréa (2)
Authors:theo
Date: 2017-01-03
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: andrea-2
Trois nuits que j'attend  
le jour je dors  
je pense aux montagnes  
aux volcans  
aux crevasses  
et en haut de la plus haute cîme  
celle que la lumière de Dieu n'atteint pas  
il y a Andréa  
mais la voilà  
\- non, ce n'est pas elle  
\- mais je la vois déjà glissant dans l'espace comme un gant  
une ombre parmis les ombres  
sans odeur  
mais les lèvres tremblantes comme si des  
larmes  
y tombaient  
et des yeux rougis par la  
honte  
qui y a élu domicile  
c'est elle  
elle traverse le couloir  
l'heure est au drame, ne penses-tu pas ?  
Elle se retourne  
à grand peine.  
Elle est enfin devant moi.  
Je la tiens.


