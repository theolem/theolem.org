Title: signifier
Author: theo
Date: 2022-07-05
Modified: 2022-07-05
Category: poèmes
Tags:
Slug: signifier

un oiseau en envol

instable merle dans les cieux

fléchit son vol

<br>

il ne se veut plus malmené par les vents

il se veut

signifier

<br>

signifier

car plus est recelé dans les caresses

de nos lèvres humides

que dans la lumière béâte dont il se dore

<br>

secouer ses plumes

car par-delà ses ailes

le monde s'agglomère

les choses saisissent comme l'argile

petit merle

<br>

signifier

pour être digne de toustes

<br>

s'armer car

la force calcifiante qui fait tomber les nuages

s'invite déjà à la table.

<br>

petit merle

dans un ciel blanc

vole

sous les deux soleils en feu

<br>

il sait puiser dans ta voix

la force des grands vents.
