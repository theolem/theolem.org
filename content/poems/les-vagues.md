Title: Les vagues
Authors:theo
Date: 2023-08-29
Modified: 2023-08-29
Category: poèmes


l'eau se mouvoit

les vagues claquent irrémédiablement.

<br>

tout commence dans le ventre

cette sensation de café trop fort qui joue avec les ficelles de l'esprit.

Je regarde de biais la bouche entrouverte car l'air est trop fine entre mes lèvres.

Je ressens le liquide dans ma bouche, dans mes doigts, dans ma queue.

<br>

sors marcher

cherche l'accalmie, quand les nuages percent tu comprendras peut-être

<br>

les espaces clos se referment une deuxième fois

et le rythme du jour qui passe griffe au passage

écris puisque c'est ce que tu as de mieux à faire

peut-être qu'un jour en écrivant tu figeras toutes les vagues de la mer

<br>

mais je remet un coup de nageoire

qui devient courant qui devient vague

ici pas de pause

à chaque heure du jour comme de la nuit tu pourras venir entendre

le bruit des vagues

les embruns calment et attisent d'une même main

<br>

pas de pause jusqu'à ce que tout ne cesse.
