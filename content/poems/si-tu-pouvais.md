Title: Si tu pouvais...
Authors:theo
Date: 2016-03-02
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: si-tu-pouvais
Si tu pouvais tourner à droite  
pour me voir attendre  
devant ta porte  
me confondant avec l'extincteur sûrement  
si seulement je ne savais pas  
que tu bois ton café noir noir noir  
noir, noir comme ta peau  
quand tu lèves tes pattes sans y croire  
\- j'en ai gardé à jamais  
l'image d'une masse éléphantesque  
éclairé par la lumière d'une torche -  
si j'oubliais encore que tu trembles  
en t'endormant  
car tu me dis que sommeil est pour toi  
autant rèves que remords  
remords envers ta meute que tu déçois  
remords envers les tiens  
s'il n'étais pas vrai que  
quand tu te prends la tête entre les mains  
cela veut dire arrête  
de parler  
d'agir même  
arrête de m'écouter  
ta simple présence est pour moi  
un épieu qui me broie les côtes  
  
si si  
si je me taisais je te jure  
que je prendrais le premier taxi  
que je lui dirai va, va  
si jamais je reviens j'aurais ta tête  
si jamais il me tombe du ciel  
une rage qui ne saurait s'appaiser  
tu ne saurais au mieux  
qu'y faire face les sourcils hauts  
la bouche dans les paumes -  
non bien sûr  
tu rirai, comme tu ris tout le temps  
méchamment, sans coeur  
  
si si si ton périple t'ammène  
en dernier recours  
à moi  
il faudra encore qu'on me retienne  
de t'étrangler à pleines mains.


