Title: sur les lignes déjà écrites
Author: theo
Date: 2021-05-03
Modified: 2021-05-03
Category: poèmes
Tags:
Slug: sur-les-lignes-deja-ecrites

sur les lignes déjà écrites

je trace encore trois mots

ceux qui diront le mieux

les visages de celleux qui me hantent.

<br>

il y avait

cellui qui riait férocement comme la chaleur d'une flamme avant l'incendie

et puis il y avait

celleux comme des royaumes en attente scrutant les augures.

<br>

sur les lignes déjà écrites

j'aimerais tracer des visages d'hommes-femmes

mais ma main tremble

et mes doigts serpentent comme pour fuir la lumière trop crue

<br>

et mon cœur

ensablé

se fond

dans le désert.
