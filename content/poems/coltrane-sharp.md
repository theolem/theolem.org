Title: Coltrane sharp
Authors:theo
Date: 2014-10-16
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: coltrane-sharp


Si c'est ce que j'étudie mon frère

c'est que l'âme elle-même faiblit vacille

hors du CHAMP

CHANTE pour moi mon frère en gris

des temps meilleurs et qui traquaient la tumeur

de temps tombés en désuétude

toujours sont MUETS

MUÉS

MURÉS claustrés cloîtrés

mon frère en noir mon frère en sombre

étrangle moi mon frère et là seulement

je pourrais finalement

endurer les armes (plutôt que les miasmes)

cracher à terre et te serrer

plutôt que FÉROCE

FAIT ROCK

FAIT ERRONÉd'une mode crasse


