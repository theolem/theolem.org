Title: Que celui...
Authors:theo
Date: 2015-09-26
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: que-celui
Que celui qui n'a jamais entendu à sa fenêtre  
la pluie au matin froid  
pour ouvrir les yeux sur du béton sec  
enveloppé d'une brume de lumière aux limites du songe  
me jette la première pierre.  
Sais-tu qu'il existe un froid mordant que j'aime  
où je me sens aimé  
où les secrets qu'on me chuchote se boivent sans sucre  
hors des torrents d'alcool en eau vive  
ancré et doux  
joint aux deux bouts  
qui mendie du matin au soir par pauvreté d'ambition  
et qui tombe en longue et belles cascades  
jusqu'au jour où -  
où quoi ? Après toi après tout  
quel esprit mesquin pourrait aveuglément m'enlever  
remplacer mes bancs de pierre par des douches fraiches ?  
Ce que je veux c'est simplement ce pli dans la terre  
et que tous ceux qui n'ont jamais entendu la pluie  
à la fenêtre me jettent la première pierre.


