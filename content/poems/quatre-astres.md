Title: Quatre astres
Authors:theo
Date: 2014-12-14
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: quatre-astres
Je me souviens de Mercure,  
noire à mes yeux mais au coeur chatoyant.  
Tu croîs dans mes entrailles  
en glissant tes bras dans les miens  
tes doigts dans les miens  
Mercure qui m'atteint  
m'attise et me murmure,  
oui, nous nous souvenons de Mercure.  
  
Je me souviens de Io,  
qui se jette aux regards en rivant son ombre au sol.  
Io la recroquevillée  
aux yeux si écarquillés  
qu'ils voient la nuit percer dans le noir.  
Io qui se hurle à elle-même  
en se frappant les côtes  
d'un coup d'oeil tordu  
comme des rides sur la peau.  
Je me souviens de Io.  
  
Puis je me souviens de Neptune  
celui ou celle qui m'appelle encore  
toujours  
un appel comme une scie qui vibre contre l'archer  
une longue note qui se tord  
s'érige et se plie lorsqu'un songe apparaît  
mais jamais ne se coupe, comme nous  
du moins le fûmes  
et souviens-toi encore, Neptune.  
  
Et je me souviens de Saturne  
qui m'ignore  
à la manière d'un sage.  
Je passe ma main sur son front  
anguleux et râpé, mais calme  
et tente de l'embrasser pour mieux  
traverser  
mais je ne puis qu'en conclure  
que nous nous souviendrons de Saturne.


