Title: Après
Authors:theo
Date: 2023-04-05
Modified: 2020-05-07
Category: poèmes

on m'a regardé avec le sourire du banquier qui te broie la main.
<br><br>
à la sortie

c'est dans les feuilles que j'ai senti

comment vibraient les fondements de la terre.
<br><br>
je les emmerde

telluriquement.
