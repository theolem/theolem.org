Title: il est des histoires
Authors:theo
Date: 2023-03-23
Modified: 2023-03-23
Slug: il-est-des-histoires
Category: poèmes

il est des histoires

qu'on ne raconte qu'en chuchotant.

<br>

celle qui m'habite

c'est celle des heures

à façonner ce que la douceur fait de mieux,

<br>

écouter comme on dit qu'on aime

dans le flottement

dans ce qui bruisse.
