Title: Avant
Authors:theo
Date: 2023-04-05
Modified: 2020-05-07
Category: poèmes

tout ça c'est très con mais je ne sais rien faire d'autre
<br>
<br>

autre que me submerger dans des questions fractales

j'ai les yeux grands ouverts pour respirer cet air où tant de paroles ont volé

mais l'orage est sombre

et les vents sont cruels

mon cœur bat et mon torse vibre des excitants que la rage sécrète.
<br><br>

autre que suivre les longues traditions humaines

comme on tire sur les clopes des autres

sans question

quand l'esprit est trop voilé

une fois la tempête calmée.
<br><br>

autre qu'errer

mais une fois on m'a glissé un autre mot

qui voulait dire se débattre dans le flot des choses tout en patientant que fleurissent les pensées

qu'est-ce que ça pouvait bien être ?
