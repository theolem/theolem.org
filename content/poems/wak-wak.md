Title: Wak wak 
Authors:theo
Date: 2017-01-27
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: wak-wak
Une douleur me presse étrangement.  
Dans le côté droit, dans mon cerveau.  
Tellement ténue, tellement compacte  
qu'à l'intérieur il pourrait y avoir un petit chien nerveux  
ceux qui bavent plus qu'ils ne boivent.  
Il est au bord de la folie.  
Le pauvre.  
Je suis malade  
non ?  
Je ne suis plus  
temporairement  
que l'arrière-boutique d'un ancien rêve.


