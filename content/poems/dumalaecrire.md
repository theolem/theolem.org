Title: J'ai du mal à écrire
Authors:theo
Date: 2022-07-14
Modified: 2022-07-14
Category: poèmes

j'ai du mal à écrire

mais je pense à toi.

<br>

l'autre fois

contexte nuages cisaillants et chaleur fumante

j'ai voulu soudain que tout soit plus simple

j'aurais voulu

que tout soit plus simple.

<br>

j'ai du mal à écrire

mais je pense à toi.

<br>

le soir

les sourires estivaux et l'oubli que la vie est crade

un instant

une photo qui m'a attaqué à la gorge

ma soirée dans la mort d'un champ de naines blanches.

<br>

j'ai du mal à écrire

mais je pense à toi.

<br>

les jours s'évident et je les arpente

et je remplis mes nuits quand fuient les couleurs

parmi toutes ces voix qui bourdonnent dans l'air

tu m'as fait glapir

et l'air avait fissuré mes organes

.

je calme la toux avec ton souvenir

la soumission quotidienne reprend

bordel

j'ai du mal à écrire.
