Title: Cordes de chanvre
Authors:theo
Date: 2017-05-05
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: cordes-de-chanvre
Cordes de chanvre tendues loin au-dessus du bastingage.  
Je ressens à présent plus ce qui est au loin  
ce qui ne luit même plus  
les souvenirs recroquevillés  
délaissés  
je les ressens en moi car ils m'ont eu à leur botte.  
Depuis  
j'ai ficelé mes valises  
je les ai jeté dans le premier  
rafiot qui ne me semblait pas  
trop sain  
j'ai laissé la fumée acre des pétards  
me poncer les bronches et les poumons  
car je sentais  
qu'il fallait que certaines forêts brûlent.  
Rome a versé sur sel  
sur les plaies de Carthage.  
Et aujourd'hui je verse de l'encre -  
puisse-tu ne jamais revivre.


