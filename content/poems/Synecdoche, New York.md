Title: Synecdoche, New York
Authors: theo
Date: 2020-08-19
Modified: 2020-08-19
Category: poèmes
Tags: 
Slug: synecdoche-new-york

parfois

le vent souffle

et je ressens que tout

tous les rouages de mon esprit

se sont arrêtés

je ressens ce vide

créateur

et potentiel

je suis devenu 

une ville endormie


<br>

Écouter la voix et le vent

se lever

marcher

puis s'asseoir et contempler.

Puis écouter le vent

\- la voix, elle, 

est partie.