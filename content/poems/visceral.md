Title: Viscéral
Authors:theo
Date: 2017-05-05
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: visceral
Hier soir j'ai eu besoin de te demander si tu croyais.  
Ce matin tu m'as répondu  
que la meilleure manière  
de malaxer une argile dure  
est de garder ses paumes plaquées contre la terre en  
enfonçant petit à petit les pouces ou plutôt  
en laissant la terre épouser  
la forme de sa peau.  
Que dois-je faire de cela ?  
Ce midi je buvais mon café à la fenêtre  
et le soleil brillait fort. Ce midi  
le soleil brillait fort et les merdeux hurlaient  
sous ma fenêtre. Ce midi  
les merdeux hurlaient sous ma fenêtre et moi  
je prenais mon café sous le grand soleil.  
Mais - me croiras-tu ? - quand j'ai pris la  
première gorgée, elle s'est transformée en sable  
\- j'ai hurlé de douleur.  
Mon plombage à la molaire gauche s'était détaché.  
J'ai craché du sang dans l'évier.  
Et la douleur m'enveloppait de ses paumes moites  
pendant que je suais en épousant les pores  
de ses doigts.  
Ce soir j'aimerais que tu saches :  
j'ai entendu, mais ce que j'ai entendu  
je ne l'ai pas compris.  
Rien ne sera jamais plus beau que le distant  
et tes mots sont les constellations du ciel.


