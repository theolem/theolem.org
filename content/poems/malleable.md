Title: Malléable 
Authors:theo
Date: 2017-05-05
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: malleable

Je regarde le Rhône  
j'écoute le lent bourdonnement de Lyon  
et j'attend que tout se meuve en ce sourd  
cet infime  
ce profond  
cette vibration de cosmos  
j'attend d'enfin sentir la résonnance des choses  
qu'elles me disent toutes à l'unisson  
tu n'es rien  
\- je ne suis rien -  
tu ne pèses rien  
\- je ne pèse rien  
si je marchais dans la neige, mes pas ne laisseraient pas de trace.  
Je suis malléable, sais-tu  
à quel point ?  
Éminémment  
malléable.


