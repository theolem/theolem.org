Title: Mer de la tranquilité
Authors:theo
Date: 2016-09-20
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: mer-de-la-tranquilite
Personne ne veut de moi, et je ne veux pas d'eux.  

J'ai un jour fuis

j'ai écarté des rideaux comme des paupières

et j'ai fuis

si vite que les grêlons m'ont fait mal

en passant par les nuages

puis j'ai fuis par les astres

si tristes que leurs roches rageuses

comme de grands visages stupides

m'ont brûlé

\- ils m'en voulaient

de rendre mes clés

à l'hôtel des rêves.

  

J'ai atteins les limites du monde connu.

L'univers observable se termine à mes pieds.

Encore un pas

et je plongerai hors - hors -

et ce sera comme un neurone cancéreux

qui se débât dans la matière blanche visqueuse

brise le crâne

coupe le cuir chevelu

et s'évapore.


