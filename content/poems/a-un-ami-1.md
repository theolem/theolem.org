Title: A un ami (1)
Authors:theo
Date: 2014-11-02
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: a-un-ami-1


1\. S'il est seulement de bonne constitution d'âme

que de s'insinuer ainsi en l'autre,

j'aime à me savoir seul – sans toi ni personne

alors que tous les étaux se resserrent

alors que le marteau file à l'enclume

et que mes pavillons se hérissent déjà au grondement.

  

2\. Ami, qu'es-tu ? Ainsi prélassé aux portes

de mon irritable peau à vif, de mes

yeux rougis pas le sable ardent, pourtant,

pourtant tu ne semble être qu'un, pourtant

tu m'attise sans larme, tu me vise sans lame et

aussi – tu dors au cœur des contradictions

comme un sourire sur la face hargneuse du monde.

  

3\. Le temps viendra où les yeux se baisseront.

Le temps viendra où tous les Icares brûleront leurs ailes.

Le temps viendra, et les joubarbes me recouvreront,

mais je pense que toi, vénérable et infantile,

jamais tu ne te vaporisera.

  

  


