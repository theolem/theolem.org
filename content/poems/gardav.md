Title: Gardav
Authors:theo
Date: 2018-05-21
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: gardav
Clés en main, gaz en tête  
jaillissez des fissures  
vous les autochtones.  
Comme on sent lorsqu'on inspire  
l'air que vous respirez  
comme on sent l'interdit transpirer !  
  
Dés en main maintenant.  
Un loquet saute, un autre  
cède.  
Il n'est plus temps de rien.  
Nous n'aurons pas le loisir de nous  
attarder. C'est déconcertant  
de me tenir si près  
de vous mes pères,  
et d'aspirer votre aura qui  
se dégrade en moite, puis en âcre.  
  
Pris en main, acier sous vos doigts.  
Vous me fixez vaguement  
comme si d'un coup s'étiolait votre  
si belle morgue.  
J'en piocherai un morceau  
si les larmes ne se devinaient pas  
derrière vos paupières.


