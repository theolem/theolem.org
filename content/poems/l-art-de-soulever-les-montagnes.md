Title: L'art de soulever les montagnes
Authors:theo
Date: 2017-10-02
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: l-art-de-soulever-les-montagnes
Le jour se lêve plein est  
et quand les premiers rayons me mordent  
arrêt sur image.  
Que le soleil accroche des hameçons  
aux fils qu'il m'envoie  
qu'on me perce de part en part  
et qu'on tire mon corps inerte jusque là où les astres fondent.  
J'ai l'impression  
du moins parfois  
que rien qui ne gît en cette pesanteur  
ne saurait me faire vivre.  
Non  
placez-moi face au plus Grand des Grands  
posez mon cul à ses pieds  
celui que personne ne conteste  
celui dont la seule proximité fend le crâne.  
Il m'en faut tant  
il m'en faut tant  
il m'en faut tant.


