Title: jézed
Authors:theo
Date: 2023-10-08
Modified: 2023-10-08
Category: poèmes

l'autre fois sur scène

il y avait des gens comme moi

mais qui gueulaient gueulaient gueulaient

et devant un public de trente à l'affût

parure sur la peau tendue

la dalle puait cette sueur qu'on déverse

et les sons étaient difformes

je me faisais chier entre les sets

dans la nuit

les lumières de la terrasse étaient crues

tout le monde fumait

quelq'  ¦  gueulait et on rentrait

ça hurlait ça s'agitait ça sautait

ça secouait mes idées et mes structures

et la musique c'est là pour ça

pour être traitée comme une énigme

quand c'est un propos ça se calcifie

chiant comme un unisson de silence

je suis rentré à vélo mes oreilles au froid

ça tournait dans ma tête

habill ¦ s  de sons parlant en images

c'était comme une grande rature sous laquelle on entrevoit des lettres

de ¦  à moi

le son ne s'échappait pas

ça restait pesant sur nos têtes

le fracas et les questions.
