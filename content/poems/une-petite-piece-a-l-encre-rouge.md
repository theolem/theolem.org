Title: Une petite pièce à l'encre rouge
Authors:theo
Date: 2014-12-14
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: une-petite-piece-a-l-encre-rouge
C'est une petite pièce  
à l'encre rouge  
habitées par un chêne et un tilleul.  
Les feuilles ont été balayées et oubliées  
balayées  
puis oubliées  
le vent et les marées  
l'étang de ses pensées  
les racines s'enroulent autour des briques de grès  
puis s'étendent  
s'enroulent et se détendent  
le chêne ravi  
le tilleul en ruine,  
l'étang de ses pensées se vide.  
  
C'est une petite pièce à quatre murs  
à l'encre rouge  
où le sol est haut et le plafond est bas.  
L'odeur d'écorce pèse sur la peau  
comme un hurlement de rage qui te  
prendrais aux os  
dans le lointain.  
  
C'est une petite pièce chaude à quatre murs  
à l'encre rouge  
terrée au fond d'un étage  
où l'Homme est grand  
et les portes sont bleues.  
Tu y viens dans mes rêves  
et tes yeux bleu sale engrangent la lumière  
tes bras sont croisés  
ta forme est repliée  
l'étang de tes pensées  
la brume vers les nuées  
dans mes dans  
mes rêves  
ton âme s'irise et traverse la  
porte, et derrière tu y vois  
  
une petite pièce à l'encre rouge.


