Title: On n'entend pas l'eau qui coule
Authors:theo
Date: 2017-02-13
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: on-n-entend-pas-l-eau-qui-coule
L'autre fois quand tu me dévisageais  
au travers de cent pas d'eau vive  
tes lèvres auraient aussi bien pu être  
mortes.  
À un premier jet de pierre  
je me serais noyé, mais un second  
je pensais  
m'aurait amené jusqu'à toi.  
Mais on n'entend  
pas  
l'eau qui coule.  
On n'entend que les lourds pas des roches  
la marche du monde  
le bruissement animal  
on n'entend  
pas  
l'eau qui coule  
seulement ses millions de fissures qui saillent.  
J'ai grandi - un peu.  
Je n'aime plus parler si je ne peux parler au monde  
je n'aime plus ce qu'on en tire  
mais quand mes mots tout juste nés de ma gorge  
me sont arrachés par le bruit ambiant  
je ne sais plus si je suis  
fasciné de te voir pourtant  
ou attristé par mon propre vide.


