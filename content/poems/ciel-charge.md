Title: Ciel chargé
Authors:theo
Date: 2018-06-11
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: ciel-charge
Ciel chargé au sommet  
de mon crâne  
tu gémis, des draps enfroissés, dans l'air suspendu  
tu enroules tes bras à mon cou  
et tu gémis  
oh  
ciel chargé  
jamais gris pourtant  
m'éveillant à ta forme  
    à ton son  
           à tes seins  
         oh  
       à tes rires   
    atterrir  
materner  
mes derniers  
ciel jamais gris m'éveille au plafonds, chez toi,  
rempli d'ombre, à en déborder  
     le volume de cette chambre  
pesant sur mes yeux  
et je me sens, parmi la certitude de t'aimer  
     comme un prototerrien  
     comme en retard  
     d'une minute  
     d'un cran.  
 Pas de clôture en moi. J'ai beau  
baisser les yeux sur toi  
me forcer à te fixer et à t'ingérer toute -  
comprends-tu que tu es encore si vive,  
moi, si je te comprend  
c'est parmi  
mes fantômes.


