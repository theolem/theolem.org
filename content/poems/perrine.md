Title: Perrine
Authors:theo
Date: 2015-02-16
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: perrine
Pour toutes ces langoureuses mais câlines  
enjambées qui m'épinent  
Perrine  
puis-je être toi puis-je avoir vent des cimes ?  
  
Toi comme une pierre qui roule mais jamais ne dîne  
du moins j'aime à le croire - car qui  
au bouts des doigts  
ne préfère pas  
un léger signe ?  
peux-tu me dire pourquoi je m'échine  
à croître en toi  
je suis un bois  
tentant de prendre  
racine Perrine au creux d'un jour  
je t'ai trouvé là lovée contre mon bras  
mais voilà que tu te défiles  
pour danser sur les charbons chauds  
pour te refléter dans l'océan  
pour battre un monde que toi seule dessine  
et peut-être même pleurer la bruine.


