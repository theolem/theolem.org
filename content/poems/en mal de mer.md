Title: en mal de mer
Authors:theo
Date: 2020-11-18
Modified: 2020-11-18
Category: poèmes
Tags:
Slug: en-mal-de-mer



en mal de mer

j'arpente le sable écumant.

à mes tympans fouette un vent vrillé

ma voix enrouée

éteinte

car là-bas

les strates de nuages s'empilent

et la tourmente s'amplifie

les torrents ivres de rage sourde

battent

la guerre, tambours

la colère, labour.

<br>

émerge la pointe acérée

la plus haute tour dont l'aiguille siffle

du son des malmenés.

<br>


mes pieds se sont enfoncés

le magnétisme de la chape de plomb

m'écoule

elle m'avide elle m'arrime.
<br>


le corps du noyé est baladé par un milliard d'embruns

mais la main agrippée aux roches figée le retient.

ses yeux fouillent les profondeurs

et la mort qui s'échappe de sa bouche vient sonder les abysses.

<br>


un ravin où rien ne pousse

qui pourrait contenir un monde.

<br>


le ciel mouvant miroitant distillant des ricochets

d'immenses lueurs qui ne dévoilent plus rien.

<br>


niché si froid si profond

l'édifice introuvable attire pourtant son regard.

c'est ça qu'il cherche

et quand il l'aura trouvé

il éclatera dans une multitude de bulles d'un long rire

et sa joie éclora si haut

qu'elle calmera les vents

réchauffera les cœurs

<br>


et on dit que les égarés

levant les yeux de leurs froids trottoirs

sans trop savoir pourquoi

porteront un instant son hommage.
