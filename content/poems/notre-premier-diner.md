Title: Notre premier dîner sans toi
Authors: theo
Date: 2021-01-01
Modified: 2021-01-01
Category: poèmes
Tags:
Slug: notre-premier-diner-sans-toi

à la fin

les bâtiments tremblaient un peu

projettant des ombres incertaines

sur tes joues creusées.

<br>

tes fils et tes filles t'ont veillé

tous ont dit combien ta vie était longue et belle

et nos voix ont tremblé

un peu.

<br>

je me suis dis

alors que mon adelphe allumait le samovar

et que la journée tirait sur sa fin

que c'était un peu comme ça que je voulais partir

moi aussi :

un long et beau moment

qui s'éteint

et la cire qui s'écoule

et qui finit par se réaranger discrètement

et tout le monde qui finit par te faire une place

et tu es enfin accepté

pleinement

un peu.
