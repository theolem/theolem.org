Title: rien ne sera jamais plus en place si ce n'est maintenant
Author: theo
Date: 2022-10-29
Modified: 2022-10-29
Category: poèmes
Tags:
Slug:

Ses cheveux dans le vent

comme le ressac

comme le vent quand il rechigne

--

Il avait fui alors qu'elle hurlait en silence

quand rien ne pouvait plus murer ses angoisses

alors il avait fui.

Depuis lors il n'avait pas su passer un jour

sans qu'une écaille ne lui grève l'oreille.

--

Ses cheveux comme l'hiver qui toquait à la porte.

Quand les forces qui le tiraient au sol

soudain parurent vaines

quand soudain

--

Sans un bruit il ferma la porte

et les sens et la montagne l'enveloppèrent.

Sans l'alcool

il n'aurait jamais su s'échapper.

Pourtant le pôle magnétique de la nuit l'attirait

alors que le son se dissipait

quai de l'Isère.

--

Quai du Rhône

Jeûne mon amour car aucun sacrifice,

que tu sévisses en des lieux lointain

ou qu'à domicile tu pourrisses,

jeûne ou bien subit ma tristesse.

--

Sans un son j'ai vécu en ton souvenir

un jour j'étais à Abidjan

la ville m'ouvrait ses lèvres mais j'ai vu ta bouche

cher⋅e toi

sans un son c'est ta bouche que j'ai vécu.

--

j'ondule en des lieux

où peu de mots comptent.

--

j'espère que je serais

ce que tu pensais que je serais

ce que tu pensais que je serais

ce que tu pensais

ta peau est ferme

tu m'étouffes mais je te cherche

je te veux que veux-tu

dans le port d'Abidjan

c'est bien tes yeux que j'ai vu.

--

Une semaine plus tard rien de tout cela n'était en fait arrivé. Il avait reçu un mail de son ex lui annonçant de bien aller se faire foutre car au final rien ne justifiait cet attachement au plus ténu de tous ces fils qu'on tisse au fur et à mesure que la vie se déroule, et ielle ne serait pas la tension qui l'ancrerait au monde. Ielle lui souriait avec aplomb par des mots de fer, et la distance qui en rayonnait lui brûlait les doigts. Le port d'Abidjan décidément le toisait d'un air bien moqueur.

--

C'EST DONC ÇA QUI TE FAIT DOUTER JEUNE PARLEUR

C'EST DONC CETTE EXCUSE QUE TU LAISSES SUR LE PAS DE MA PORTE POUR NE PAS AVOIR À PARLER

SONGE

SONGE DONC QUE CE N'EST RIEN

QUE TU N'ES RIEN ET QUE RIEN N'EST RIEN SI CE N'EST L'ENVELOPPE QUE TON HISTOIRE LAISSE APRÈS SA MUE

QUE LES VILLES BRÛLENT

ET QUE L'ESPACE POURRISSE

QUAND TU EN AURAS FINI

SEULES RESTERONT LES BRISURES D'ÉPINES

ET LES ÉPINES D'ÉCLATS.

--

C'est donc cela que tu laisses devant ma porte.

Comme j'aurais aimé te dire

que j'aurais aimé être là

que tu es plus pour moi que des tonnes de lest sur mon cœur

oublie ces voix qui te tirent vers les glaciers alpins

viens car mes bras sont ouverts

ma peau est ferme

je ne tremble pas

rien ne sera jamais plus en place

si ce n'est maintenant.
