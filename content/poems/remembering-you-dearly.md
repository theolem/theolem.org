Title: Remembering you dearly
Authors:theo
Date: 2017-05-18
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: remembering-you-dearly
Tu n'es plus pour moi que cendres  
mais ta voix au loin perce encore  
asynchrone car les vagues de temps la bouscule  
dans mon esprit  
mais ta voix perce encore.  
Mais faut-il quelque-chose de grand  
d'immensément grand pour  
percer cette peur cette rancoeur  
\- je n'ai plus pour toi des fois que  
  rage écumante de sueur et cela  
  gerce mes joues et cela irrite  
  mes paumes que mes ongles  
  arrachés raclent en toute furie -  
 ou faut-il  
savoir au plus  
tendre  
de soi  
que tu n'es plus pour moi que cendres  
et que ta voix n'est plus pour moi qu'embruns  
et que je t'en chérie de plus belle  
d'avoir tant  
vécu ma vie.  
Je me souviens doucement de toi  
sans sourire  
juste pour te sentir inonder à nouveau mes veines  
et cette journée d'un coup s'illumine en moi.


