Title: Chat noir, ours blanc
Authors:theo
Date: 2016-12-28
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: chat-noir-ours-blanc
Chat noir, ours blanc  
sans prise au vent  
dis-moi l'enfant  
si je suis grand  
ou bien sinon  
lêve-toi d'un bond  
va de l'avant  
et pour de bon  
  
Ours blanc, chat noir  
il se fait tard  
mais pas encore  
vraiment assez  
tard pour rester  
là à ton bord  
j'en ai assez  
j'en ai assez  
chat noir ours blanc  
j'ai pris le temps  
d'écrire l'histoire  
et maintenant  
chat noir ours blanc  
me laissera-t-on  
prendre mon temps


