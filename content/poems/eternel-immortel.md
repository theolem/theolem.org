Title: Eternel, immortel
Authors:theo
Date: 2016-11-28
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: eternel-immortel
il fait si sombre  
qu'il marche en silence  
pour ne pas froisser  
le froid qui le berce.  
  
il est si sombre  
cet os qui ne veut pas mourir.  
Il m'enterrera, il enterrera mes enfants.  
On a voulu lui arracher son secret  
et il a parlé  
non, tout juste soufflé  
sa réponse  
sous la forme d'une phrase  
si longue  
si fine  
que le temps que sa langue la déroule  
nous étions tous trop vieux  
pour nous rappeler de comment elle avait commencé.  
  
J'aurais pensé que si - moi -  
j'avais vécu tant  
alors mon sang aurait quitté mes vaisseaux  
et m'aurait gonflé comme une baudruche.  
J'aurais  
explosé  
mais lui a su - je ne sais -  
il eut été mélodie  
que sa dernière note  
aurais pour toujours résonné  
mais les murs trop éloignés  
pour jamais ricocher.  
  
il est si vieux maintenant  
que le temps l'a perdu.  
il est flou même aux yeux  
des hommes  
il aurait pu être cinq hommes  
que je ne le saurais pas.


