Title: Andréa (3)
Authors:theo
Date: 2017-01-03
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: andrea-3
Andréa me fait mourir  
Andréa m'enflamme et m'attise  
je vogue sans voile sans elle  
sans sud sans nord  
sans cesse pourtant  
Andréa me flingue  
Andréa me donne et me rend  
au monde  
j'ai vu Andréa par la serrure  
je me suis immiscé  
j'en oubliai mon corps pataud  
\- Andréa me manque  
Andréa jamais assez toujours trop peu -  
elle fume sur le balcon  
un homme lui fait face  
\- Andréa me fuit  
Andréa Andréa  
elle le pousse il tombe  
oh il tombe  
elle fume sur le balcon  
ses lèvres tremblotent je crois  
Andréa  
je suis pris du désir  
de tout revivre avec elle  
et d'enfin sentir  
qu'en mes entrailles  
Andréa est là  
et que jamais elle ne s'en ira.


