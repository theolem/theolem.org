Title: on se perd
Authors:theo
Date: 2023-09-18
Modified: 2023-09-18
Category: poèmes

il y a tellement peu de textes qui parlent de doute.

<br>

le silence interne est paralysant pour l'esprit cotonneux

lire des brochures et se convaincre que c'est le dehors qui assène

ce vide

que c'est la pénétration de la ville violente sous la peau

<br>

j'ai perdu le sortilège qui me faisait dévaler les pentes

moteur câlé

le paysage frappe ma rétine

les montagnes se tiennent à une distance respectueuse

perdues dans leurs propres monologues.


<br>

sont rompus les liens qui ne contraignent pas

je ne plois plus le cou ou rarement

on m'a vidé de tout ce qui me tailladait les trippes

<br>

à distance reviennent les pensées grandes

quitté par les géants

dans la nuit qui précédait l'émeute.
