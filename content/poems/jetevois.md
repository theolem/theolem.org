Title: je te vois
Authors:theo
Date: 2024-11-30
Modified: 2024-11-30
Category: poèmes
Tags:
Slug: jetevois

je te vois dans un immeuble au bizarre goût de roche

</br>

les bâtiments - les fusions soudaines entre les gens - les rues qui vont partout mais tout tu vas toujours pareil - cette identité impossible entre ce que tu es et ce que tu dis

</br>

tout cette litanie s'enroule autour de toi

fait le siège de ton enfance

</br>

je te vois c'est pas que t'as tort

\- il y a suffisament de gueules qui s'acharnent à dépecer jusqu'à ton odeur et tes mains

suffisament de creu dans la terre pour en garder chacun⋅e un peu en soi -

</br>

c'est pas que t'as tort c'est que t'as oublié l'espace et les rayons bruts du soleil

les corps qui s'élancent et le chaos qui murmure en cercle tout autour

les vagues quand ça se mouvoie - tout le temps - et les crash énorme à vitesse géologique

</br>

ça fonctionne pareil je te jure

et tu exploses de grande lumière déjà

je te vois j'aimerai tendre le temps qui nous sépare
