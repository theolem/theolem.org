Title: sens
Authors:theo
Date: 2023-10-06
Modified: 2023-10-06
Category: poèmes

un grondement de fond de gorge répond à la fragilité sans fin.

<br>

un jour sans fin répond aux effréné⋅es d'hier

<br>

un empilement de sons complexes répond au silence distrait

<br>

un silence rogné répond aux quelques phrases qu'on a su écrire.

<br>

c'est les échos de ma vingtaine égarée qui répondent aux grands arbres

le chlore odorant qui répond à la rue qui hurle

quand je bois c'est le séisme des larmes qui répond à la moindre photo

la vanité qui réponnd à l'affirmation

c'est la sueur qui répond à l'été

la question qui répond à l'épiphanie

mon corps qui répond à une main

ce qui émane répond à ce qui bout

<br>

mes mots qui répondent au vide de mots.
