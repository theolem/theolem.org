Title: Trois fois rien
Authors: theo
Date: 2021-03-28
Modified: 2021-03-28
Category: poèmes
Tags:
Slug: trois-fois-rien



deux chauds amis

marchant sans bris

ça se déroule

sans échauffouré

mais un de mille mots fuse

et change le ton

qui maintenant déferle

comme du sable charié

par un mauvais crachin.

<br>

en chien, des mots dans les lèvres

et le noir des yeux blanchis

ce mépris hante et entâche les deux hères

qui repartent seuls, en silence, dans la nuit.
