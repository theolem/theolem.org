Title: PØLÅIR
Authors:theo
Date: 2014-11-03
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: polair


Comme une bribe

de corsage de nacre pourpre

effleurée d'un doigt

comme parcourue d'un champ si

enrobant si sibérien si

si claquant si

si PØLÅIR – voyez

elle s'ouvre enfin le torse

(cheveux rouge, lèvres pâles) et

déverse (seins ronds, ventre plein)

déverse (longues cuisses, mollets fins et laiteux)

  

je les vois :

ils remontent à loin, ces airs impériaux

et hauts comme un aigle

ils remontent à loin ces airs de pianiste

dressés comme des tentures aux murs

je les vois et les aime

je les ai aimé.

  

can I call you mine ? ma belle ortie

d'encre sentant le lait et

l'ennui

toujours les deux yeux vers la gloire

haute dans la stratosphère

là où les particules de glace te projetteront au nuages,

je t'aime et je te décline.

sous ton armature de libellule aux mille côtes

sous ta peau où le cuir doux et tanné se

mêle à la soie d'araignée

et ta joue ( PØLÅIR, PØLÅIR ! ma nordique, ma Scandinave !)

se trouve et je le sais un

milliard d'yeux et un million d'oreilles fous et

avides de crainte.

  

ou alors – je le sais bien -

c'est la veillée des nuits froides à se

pelotonner dans le vent, à hurler aux steppes

les pires gémissements.

  

je ne sais plus.

did you call me yours ?

si nous séparons les hémisphères

  

tu gardera le Nord – moi, le Sud.


