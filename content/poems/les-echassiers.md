Title: Les échassiers
Authors:theo
Date: 2016-01-26
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: les-echassiers
Les échassiers sortent en grande pompe ce matin.  
On leur a à peine  
tiré les plis de leurs grands manteaux couleur brune  
qu'on les presse déjà à la porte  
dans leur somnolence du petit matin.  
Ils sont une tâche d'encre  
appliquée devant chaque maison  
avec application  
et tous se détachent dans les hautes brumes  
lents comme des cafards  
hauts comme des pylônes  
apportant grâce plutôt que pitié quand peut-être  
le bon sens leur interdirait.  
  
j'aimerai un jour courir  
courir courir et hop -  
grimper aux échasses de l'échassier  
m'agripper comme un enfant aux cols de plumes éparses  
et monter plus haut encore  
franchir des monts des homoplates  
des lombaires et des cervicales  
un lobe occipital  
et m'emparer des mâchoires et des lèvres  
pour moi ! - comprenez vous  
POUR MOI  
et alors quand je sauterai dans le vide  
des miles au-dessus de moi, des miles tout autour  
avant que je ne touche le sol  
voir mes fémurs s'élancer gaiement  
voir mes tibias lêcher le sol  
mes lombaires mes cervicales se voûter afin  
de mieux scruter en bas  
et si je n'attrape pas d'étoile du moins puis-je leur sourire.


