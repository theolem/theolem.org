Title: émerger
Authors:theo
Date: 2023-09-12
Modified: 2020-09-12
Category: poèmes

hier

nuit

tu as élevé ton corps

et je t'ai retrouvé⋅e.

---

le ciel enfin s'assombrit

la pluie va tomber.

elle me dit : "regarde comme

tout finit par se figer

et tout dégouline."

---

je traverse la perdition


mais je me souviens de toustes

infiniment beau⋅elles

au sommet de l'existence.

---

la mer est descendue tirée par la lune

je reviens à moi

j'inhale.

---

mon corps mon sexe la sueur mon envie ma rage et la pensée infime d'une vie possible

enfin resynchronisés.
