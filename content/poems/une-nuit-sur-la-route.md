Title: Une nuit sur la route
Authors:theo
Date: 2016-11-28
Modified: 2020-05-07
Category: poèmes
Tags: 
Slug: une-nuit-sur-la-route
et vient l'entrain d'aimer en somme.  
Où va-t-on donc ? Qui sait au fond  
s'il fallait prendre vers l'horizon  
ou si l'homme blanc aux doigts de chrome  
  
juché devant le volant sombre  
un oeil sur moi, l'autre sur la mort  
laissant faire tous les autres à bord  
goûtait tout seul à cette encombre  
  
de pointer de l'oeil aiguisé  
la gauche la droite ou l'au-delà  
l'envers l'endroit où je ne sais quoi  
barrer l'emphase posée par l'homme              ^


