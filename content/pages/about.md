Title: à propos
Date: 2020-04-24 17:01
Modified: 2020-04-24 17:01
Slug: about
Authors: theo

Ce site est réalisé par mes soins avec [un super générateur de site statique que j'adore](https://blog.getpelican.com/).

## Crédits divers

* La photo de bannière a été prise par Manon, et retouchée par son imprimante, que je salue.
* La police utilisée partout sur ce site est [Happy Times](https://www.velvetyne.fr/fonts/happy-times/) (SIL Open Font License, Version 1.1).

## Contact

Je réponds normalement [par mail](mailto:me@theo-lem.org) (ça fonctionne ?) ou bien [sur Mastodon](https://toot.theo-lem.org/@theo).

## Et sinon rien à voir mais

* j'héberge quelques services, comme un aggrégateur de flux RSS, un cloud ou encore un wiki. Ils sont plutôt destinés à mon usage personnel, mais je serais heureux de partager ça avec toi si tu le souhaites.
* [je jouais de la musique fut un temps](https://peacock1.bandcamp.com/album/all-thoughts-of-ever).
* En ce moment je fais ça :
<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1354808275&color=%23eedee0&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/grand-sablon" title="Grand Sablon" target="_blank" style="color: #cccccc; text-decoration: none;">Grand Sablon</a> · <a href="https://soundcloud.com/user-845105664/sets/jams" title="Jams" target="_blank" style="color: #cccccc; text-decoration: none;">Jams</a></div>

